import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController } from 'ionic-angular';

/**
 * Generated class for the IngredientPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ingredient',
  templateUrl: 'ingredient.html',
})
export class IngredientPage {
	public id;
	public items;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
  	  	this.id = navParams.get("id");
  	  	if (this.id == 1)
  	  	{
		  	this.items = [
		    '• 50 gm. of jelly - soaked until soft',
		    '• 400 gm. of sugar',
		    '• 1 cup of rock sugar',
		    '• 6 cups water',
		    '• Pandan leaf',
		    '• Red and green dyes'
		  ];
  	  	}
  	  	else if (this.id == 2)
  	  	{
 		  	this.items = [
		    '(A: Shrimped together)',
		    '•	300 grams of wheat flour',
		    '•	300 grams of sago flour',
		    '(B)',
		    '•	1200 grams of sugar',
		    '•	1600 ml of water',
		    '•	600 grams of beef oil',
		    '•	2 grams of cardamom, finely grounded and sifted',
		    '•	2 grams of almonds, soaked with hot water to remove the skin and sliced finely and fried with 150 ml of peanut oil',
		    '•	2 grams of raisins'
		  ]; 	  		
  	  	}
  	  	else if (this.id == 3)
  	  	{
 		  	this.items = [
		    '•	600 grams of grated coconut',
		    '•	500 grams of glutinous flour',
		    '•	350 grams of sugar',
		    '•	Water is enough',
		    '•	Salt to taste',
		    '•	Cooking oil to taste'
		  ]; 	  		
  	  	}
  	  	else if (this.id == 4)
  	  	{
 		  	this.items = [
		    '(BOTTOM LAYER)',
		    '•	2 cups of glutinous rice',
		    '•	2 1/2 cups of liquid coconut milk + a little salt',
		    '(UPPER LAYER)',
		    '•	cups of thick coconut milk',
		    '•	Headstones / 1 cup of Melaka sugar',
		    '•	eggs',
		    '•	1 t / t salt'
		  ]; 	  		
  	  	}
  	  	else if (this.id == 5)
  	  	{
 		  	this.items = [
		    '•	13 banana bananas / gold bananas',
		    '•	1 cup of wheat flour',
		    '•	2 tbsp. sugar',
		    '•	2 tbsp. brown sugar - raise the colour',
		    '•	1 tsp. salt'
		  ]; 	  		
  	  	}
  	  	else if (this.id == 6)
  	  	{
 		  	this.items = [
		    '(BOTTOM LAYER) 10-12 SMALL BOWLS',
		    '•	62 g rice flour (8 tbsp.)',
		    '•	660 ml water (2 1/2 cup + 2 tbsp.)',
		    '•	62 ml Pandan water (3 tbsp.) + 1/2 tsp. green dye',
		    '•	1/2 tsp. salt',
		    '•	1 tsp. of chalk water',
		    '(TOP LAYER)',
		    '•	25 g rice flour (3 tbsp.)',
		    '•	600 g coconut milk (2 cup + 5sb)',
		    '•	1/2 tsp. salt ',
		    '(SUGAR SYRUP)',
		    '•	250 g sugar Melaka (1 piece - finely sliced)',
		    '•	500 ml water (2 cup)',
		    '•	1 pandan leaf - in the node'
		  ]; 	  		
  	  	}
  	  	else if (this.id == 7)
  	  	{
 		  	this.items = [
		    '•	10 duck eggs',
		    '•	1 kg of granulated sugar',
		    '•	250 gm. of glutinous flour',
		    '•	100 gm. of rice flour',
		    '•	2 pandan leaves',
		    '•	5 cloves of cloves',
		  ]; 	  		
  	  	}
  	  	else if (this.id == 8)
  	  	{
 		  	this.items = [
		    '•	2 cups of rice flour',
		    '•	1/2 cup of wheat flour',
		    '•	1/2 cup corn flour',
		    '•	1 cup of sugar',
		    '•	cup coconut milk (1 cup coconut milk mixed with 3 cups of water)',
		    '•	1/4 teaspoon of salt',
		    '•	Little red colour'
		  ]; 	  		
  	  	}
  	  	else if (this.id == 9)
  	  	{
 		  	this.items = [
		    '•	20 chicken eggs (take yellow only)',
		    '•	2 kg of sugar',
		    '•	glasses of water'
		  ]; 	  		
  	  	}
  	  	else if (this.id == 10)
  	  	{
 		  	this.items = [
		    '•	500 grams of glutinous flour',
		    '•	Water',
		    '•	1 tsp. pandan leaf dye',
		    '•	pandan leaves',
		    '•	1 piece of Melaka sugar',
		    '•	Grated coconut'
		  ]; 	  		
  	  	}
  	  	else if (this.id == 11)
  	  	{
 		  	this.items = [
		    '•	1 pack of glutinous flour',
		    '•	15 pandan leaves, taken with water',
		    '•	2 coconuts, one grated coconut milk made one nucleus and another extracted coconut milk 2 cups of brown sugar',
		    '•	Enough water',
		    '•	A little salt'
		  ]; 	  		
  	  	}
  	  	else if (this.id == 12)
  	  	{
 		  	this.items = [
		    '•	300 gm. of wheat flour - sifted',
		    '•	2 pandan leaves - take the water',
		    '•	2 - 3 cups of coconut milk from one coconut bean',
		    '•	1 cup of sugar, or enough sweetness needed',
		    '•	5 eggs, beaten',
		    '•	A pinch of salt',
		    '•	A little yellow dye',
		    '•	Fried onions as an ornament'
		  ]; 	  		
  	  	}
  	  	else if (this.id == 13)
  	  	{
 		  	this.items = [
		    '(A)',
		    '•	2 cups of rice flour.',
		    '•	1/2 cup of cold rice.',
		    '•	1/2 cup thick coconut milk.',
		    '•	1 teaspoon instant yeast.',
		    '•	2 tbsp. sugar.',
		    '•	1 1/2 cups of water.',
		    '•	1 tsp. salt',
		    'INGREDIENTS OF GRAVY',
		    '•	1 cup of brown sugar.',
		    '•	1 cup coconut milk.',
		    '•	1/2 cup of water.',
		    '•	2 eggs.',
		    '•	1 cup thick coconut milk and 1 tablespoon of corn flour - brewed.',
		    '•	2 pieces of pandan ',
		    'INGREDIENTS FOR SERAWA DURIAN',
		    '•	2 cups coconut milk.',
		    '•	100 gm of durian content.',
		    '•	80 gm of sugar.',
		    '•	1/4 teaspoon of salt.',
		    '•	1 pandan leaf.',
		    '•	1 egg whites at.'
		  ]; 	  		
  	  	}
  	  	else if (this.id == 14)
  	  	{
 		  	this.items = [
		    '•	2 slices of sugar sweets (sugar Melaka)',
		    '•	1kg of wheat flour',
		    '•	250ml of water',
		    '•	1 small bottle of corn oil',
		    '•	2 bamboo sticks medium and long flat',
		    '•	1 jar of copper pot',
		    '•	1 small pot',
		    '•	1 saucer',
		    '•	1 simple basement seed filled with water',
		    '•	1 sheet of oil paper',
		    '•	1 pan',
		    '•	1 wooden spoon'
		  ]; 	  		
  	  	}
  	  	else if (this.id == 15)
  	  	{
 		  	this.items = [
		    '•	10 white eggs',
		    '•	500ml coconut milk',
		    '•	400gm of sugar',
		    '•	100gm of glutinous flour',
		    '•	150gm rice flour',
		    '•	A pandan leaf'
		  ]; 	  		
  	  	}
  	  	else if (this.id == 16)
  	  	{
 		  	this.items = [
		    '•	1 packet of rice flour',
		    '•	pieces of sugar Melaka',
		    '•	8 glasses of water',
		    '•	1 coconut',
		    '•	Little water of betel nut',
		    '•	A little salt ',
		    '•	2 banana leaves (for cladding)'
		  ]; 	  		
  	  	}
  	  	else if (this.id == 17)
  	  	{
 		  	this.items = [
		    '•	Cassava',
		    '•	Salt',
		    '•	Young coconut',
		    '•	Tomb sugar',
		    '•	Glutinous rice',
		    '•	Water'
		  ]; 	  		
  	  	}
  	  	else if (this.id == 18)
  	  	{
 		  	this.items = [
		    '•	250 gm. of wheat flour',
		    '•	50 gm. rice flour',
		    '•	100 ml hot water + 225 ml cold water',
		    '•	200 gm. of brown sugar',
		    '•	50 gm. white sugar',
		    '•	1/4 teaspoon of salt',
		    '•	copies pandan leaves'
		  ]; 	  		
  	  	}
  	  	else if (this.id == 19)
  	  	{
 		  	this.items = [
		    '•	1/2 grated coconut',
		    '•	2-1 / 2 cups of glutinous flour',
		    '•	1/2 cup of wheat flour',
		    '•	1cwn water (or enough)',
		    '•	1 tsp. salt'
		  ]; 	  		
  	  	}
  	  	else if (this.id == 20)
  	  	{
 		  	this.items = [
		    '•	1 cup thick coconut milk',
		    '•	1 cup brown sugar',
		    '•	2 tbsp. sugar',
		    '•	1 cup water',
		    '•	2 cups coconut milk',
		    '•	1 cup green bean flour (cap deer)',
		    '•	Some pandan leaf pieces are inferred',
		    '•	A pinch of salt'
		  ]; 	  		
  	  	}
  	  	else if (this.id == 21)
  	  	{
 		  	this.items = [
		    '•	Yellow eggs',
		    '•	egg whites',
		    '•	100gm of sugar',
		    '•	1/2 tsp. vanilla essence.',
		    '•	100gm wheat flour',
		    '•	1 tsp. of flour',
		    '•	1/4 tsp. baking powder'
		  ]; 	  		
  	  	}
  	  	else if (this.id == 22)
  	  	{
 		  	this.items = [
		    '•	2 cups sweet potatoes',
		    '•	2 cups of sugar',
		    '•	2 coconuts (extracted coconut milk)',
		    '•	5 chicken eggs 1 packet of sago flour',
		    '•	A little baking powder'
		  ]; 	  		
  	  	}
  	  	else if (this.id == 23)
  	  	{
 		  	this.items = [
		    '•	500g rice flour (1 pack of blended rice flour)',
		    '•	500g brown sugar',
		    '•	1 1/4 cups of water',
		    '•	1 pandan leaf (if any)'
		  ]; 	  		
  	  	}
  	  	else if (this.id == 24)
  	  	{
 		  	this.items = [
 		  	'INGREDIENTS',
		    '•	1 pack of glutinous flour',
		    '•	2 cups coconut milk (half bowl',
		    '•	Coconut milk cooked until broken oil)',
		    '•	A little salt',
		    '•	Banana leaves that have been adequately swung',
		    'INGREDIENTS OF INTI',
		    '•	1 grated coconut',
		    '•	½ cup sachet sugar',
		    '•	1 cup of sugar',
		    '•	2 Pandan leaves'
		  ]; 	  		
  	  	}
  	  	else if (this.id == 25)
  	  	{
 		  	this.items = [
		    '•	250g rice flour',
		    '•	tbsps. All-purpose flour',
		    '•	eggs ',
		    '•	200g granulated sugar ',
		    '•	400ml thick coconut milk',
		    '•	1/4 tsp. salt',
		    '•	Enough cooking oil for deep frying'
		  ]; 	  		
  	  	}
  	  	else if (this.id == 26)
  	  	{
 		  	this.items = [
		    'INGREDIENTS FOR THE DOUGH',
		    '•	800 grams of white sweet potato (in 2 large lobster)',
		    '•	1 1/4 cup of wheat flour',
		    '•	2 tsp. baking powder',
		    '•	1 tsp. salt',
		    'INGREDIENTS FOR SUGAR COATING',
		    '•	1 tbsp. of sugar',
		    '•	1 tbsp. white sugar',
		    '•	1/2 cup water'
		  ]; 	  		
  	  	}
  	  	else if (this.id == 27)
  	  	{
 		  	this.items = [
		    'INGREDIENTS FOR GLUTINOUS RICE LAYER',
		    '•	400 gm. or 2 cups of white glutinous rice (soaked for 3 hours)',
		    '•	2 cups of liquid coconut milk (same as glutinous rice)',
		    '•	1 teaspoon of salt',
		    '•	2 tbsp. cooking oil',
		    'INGREDIENTS FOR UPPER LAYER',
		    '•	2 cup thick coconut milk',
		    '•	150 mL water + 4 pandan leaves',
		    '•	(Repeated and filtered for pandan water)',
		    '•	1 cup of wheat flour',
		    '•	1 1/2 tablespoons of corn flour',
		    '•	3/4 cup of sugar (if sweet-1 cup of sugar)',
		    '•	1 chicken egg',
		    '•	1/2 teaspoon of salt',
		    '•	A little green dye'

		  ]; 	  		
  	  	}
  	  	else if (this.id == 28)
  	  	{
 		  	this.items = [
		    'INGREDIENTS FOR FILLING',
		    '•	1/2 grated coconut',
		    '•	1/4 cup of Melaka sugar',
		    '•	1/4 cups of sugar',
		    '•	2-3 pandan leaves',
		    '•	1/2 teaspoon of salt',
		    'INGREDIENTS FOR THE SKIN',
		    '•	2 cups of wheat flour',
		    '•	2 cups of simple liquid milk, an estimated 1/3 of coconuts',
		    '•	1 egg',
		    '•	4-5 pandan leaves',
		    '•	1 tsp. salt',
		    '•	2-3 green dots (optional)',
		  ]; 	  		
  	  	}
  	  	else if (this.id == 29)
  	  	{
 		  	this.items = [
		    '•	1 cup of wheat flour',
		    '•	1 cup caster sugar',
		    '•	1 cup pandan water (6 pieces pandan leaf blend with water)',
		    '•	2 cups coconut milk',
		    '•	eggs',
		    '•	A little green dye',
		    '•	As an ornament - a bit of sesame'
		  ]; 	  		
  	  	}
  	  	else if (this.id == 30)
  	  	{
 		  	this.items = [
		    '•	2 eggs',
		    '•	1 cup of sugar',
		    '•	2 cups wheat flour + 1 tsp. baking powder',
		    '•	1 cup coconut milk (make sure to mix with pandan starter1 cup)',
		    '•	A little pandan (I grind 2 pandan leaves with a little water)',
		    '•	1 tsp. ovulate',
		    '•	A little green dye',
		    '•	Grated coconut white - + 1 tablespoon wheat flour + a little salt',
		  ]; 	  		
  	  	}
  	  	else if (this.id == 31)
  	  	{
 		  	this.items = [
 		  	'INGREDIENTS A',
		    '•	2 eggs',
		    '•	1 cup of sugar',
		    '•	2 cups of flour',
		    '•	2 cup coconut milk for coconut milk',
		    '•	Beat eggs until flowers.',
		    '•	Insert the egg in the blender only and rotate',
		    '•	In another container, mix sugar, wheat flour and coconut milk, stir well.',
		    '•	Mix with egg and stir well.',
		    '•	When ready to set aside',
		    'INGREDIENTS B',
		    '•	1 cup thick coconut milk',
		    '•	1 cup pandan water (take 7 pieces of pandan leaves and then filter)',
		    '•	1/2 cup of refined sugar',
		    '•	1/2 cup corn flour',
		    '•	A pinch of salt',
		    '•	A little green apple dye',
		    'INGREDIENTS C',
		    '•	cups coconut milk',
		    '•	tablespoons of wheat flour',
		    '•	1/2 teaspoon of salt',
		    '•	How to make a sweet cup measure'
		  ]; 	  		
  	  	}
  	  	else if (this.id == 32)
  	  	{
 		  	this.items = [
		    '•	tbsp. glutinous flour',
		    '•	1 tablespoon of rice flour',
		    '•	pandan leaves - mix with water and take enough juice',
		    '•	Melaka - small pieces',
		    '•	1/2 grated young coconut - take only white',
		    '•	A little salt - to mix with grated coconut',
		    '•	A little lime water'
		  ]; 	  		
  	  	}
  	  	else if (this.id == 33)
  	  	{
 		  	this.items = [
		    '(BOTTOM LAYER) ',
		    '•	30gm corn flour',
		    '•	1 teaspoon pandan juice or green dye',
		    '•	300 ml of water',
		    '•	100gm of sugar',
		    '•	10 large pandan leaves',
		    '•	A pinch of lime meal (mixed with pandan juice)',
		    '(UPPER LAYER) ',
		    '•	15gm rice flour',
		    '•	½ grated coconut (8 oz. coconut milk)',
		    '•	A little salt'
		  ]; 	  		
  	  	}
  	  	else if (this.id == 34)
  	  	{
 		  	this.items = [
		    '•	100 g dark palm sugar, chopped',
		    '•	100 ml water',
		    '•	50 g sugar',
		    '•	1 pandan leaf, knotted',
		    '•	60 g rice flour',
		    '•	20 g tapioca flour',
		    '•	½ teaspoon alkaline water',
		    '•	Topping',
		    '•	60 g freshly grated coconut',
		    '•	Pinch of salt'
		  ]; 	  		
  	  	}
  	  	else if (this.id == 35)
  	  	{
 		  	this.items = [
		    'INGREDIENTS FOR FILLING',
		    '•	200 gram minced beef',
		    '•	1 large onion diced',
		    '•	2 slivers of garlic diced',
		    '•	2 tbsp. of curry powder (for meat)',
		    '•	Salt and sugar to taste',
		    '•	Cilantro (to garnish)',
		    'INGREDIENTS FOR BATTER',
		    '•	2 cups of flour',
		    '•	Water and coconut milk (1 to 1 part) to make smooth batter as thick as pancake batter',
		    '•	1 egg',
		    '•	A bit of salt to taste – not too much as you will have salt in the filling'
		  ]; 	  		
  	  	}
  	  	else if (this.id == 36)
  	  	{
 		  	this.items = [
		    '•	8 pandan leaves, to be washed, dried and cut into pieces',
		    '•	2 cups water',
		    '•	440 g plain flour',
		    '•	medium-size eggs',
		    '•	80 g castor sugar or to taste',
		    '•	A pinch of salt',
		    '•	1 cup coconut milk',
		    '•	A few drops of yellow colouring',
		    '•	1 tbsp. ghee, for greasing mould'
		  ]; 	  		
  	  	}
  	  	else if (this.id == 37)
  	  	{
 		  	this.items = [
		    '(A)',
		    '•	250 gram butter ',
		    '•	1 cup of sugar',
		    '•	1 cup water',
		    '(B)',
		    '•	1 teaspoon of liquid milk',
		    '•	1 cup of refined sugar',
		    '•	2 eggs',
		    '•	2 tablespoons of soda',
		    '•	cups of wheat flour'
		  ]; 	  		
  	  	}
  	  	else if (this.id == 38)
  	  	{
 		  	this.items = [
		    'INGREDIENTS OF CORE',
		    '•	1/2 grated coconut beans - take only the white',
		    '•	3/4 cups of brown sugar - if you do not like sweet, you can reduce it.',
		    '•	1/2 teaspoon salt',
		    '•	2 pandan leaves - concluded',
		    '•	1/2 cup water',
		    '•	2 tbsp. wheat flour',
		  ]; 	  		
  	  	}
  	  	else if (this.id == 39)
  	  	{
 		  	this.items = [
		    'INGREDIENT OF KUIH LOPES',
		    '•	2 cups of milk powdered milk',
		    '•	2 cups of liquid coconut milk',
		    '•	1/2 cup pandan leaf water - blended with some pandan leaves',
		    '•	1/2 teaspoon of chalk water',
		    '•	1/2 teaspoon of salt',
		    '•	Some drops of green dye',
		    '•	Grated coconut - mix with a little salt',
		    'INGREDIENTS OF GULA MELAKA',
		    '•	cakes of Melaka sugar',
		    '•	tbsp. sugar',
		    '•	cups water',
		    '•	2 pandan leaflets',
		  ]; 	  		
  	  	}
  	  	else if (this.id == 40)
  	  	{
 		  	this.items = [
		    '•	Orange sweet potatoes',
		    '•	Wheat flour',
		    '•	A little sugar'
		  ]; 	  		
  	  	}
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad IngredientPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
