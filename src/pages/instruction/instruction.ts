import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';

/**
 * Generated class for the InstructionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-instruction',
  templateUrl: 'instruction.html',
})
export class InstructionPage {
	public id;
	public items;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
  	  	 this.id = navParams.get("id");

  	  	 if (this.id == 1)
  	  	 {
		  	this.items = [
	    	'1.	Cook jelly with water and pandan leaves until dissolved.',
		    '2.	Add sugar and sugar to rock and cook again until all the sugar has fallen. When the sugar has been crushed and concentrated, turn off the fire.',
		    '3.	Filter the sugar solution and place in 2 or more containers. Add one dye in each pan. Stir well and cool.',
		    '4.	Cut the jelly by size. Don’t cut too small because it will shrink when it is sunk.',
		    '5.	Prepared pre-cut of jelly can be arranged in a tray and dried in the heat of the sun until it is completely dry.',
		    '6.	Agar-agar that already cut must often be turned upside down because it can dry evenly.',
		    '7.	Dried enough agar can be stored in a dry container.'
		  ];
  	  	 }
  	  	 else if(this.id == 2)
  	  	 {
  	  	 	this.items = [
  	  	 	'1.	Fill in material A in a large pot. Soak it with water (twice the flour and keep it for 2 days) But every morning it should be stirred and let it dry, and in the evening it is thrown out and replaced with fresh water.',
		    '2.	Cook material B to boil. Put the soaked flour (peeled off) together with the beef oil then stirred to the average to boil using a simple fire.',
		    '3.	When boiling, add the almonds, raisins and fruit pellets, stirring occasionally to avoid scorching.',
		    '4.	When the cake is cooked, that is when we break the finger on it if is not sticky, it can be lifted.',
		    '5.	Prepare pan or tray that is wiped with a little oil around it.',
		    '6.	Pour it aside, evenly pressed it to make it smooth and smooth and let it cool'
		  ];
  	  	 }
  	  	 else if(this.id == 3)
  	  	 {
  	  	 	this.items = [
		    '1.	Mix glutinous rice and grated coconut, then add some salt and water. Dismiss the blend.',
		    '2.	Heat the pan with a little oil on it (if you have no mold).',
		    '3.	Then place the dough in the mold, with a thickness of 1-2 cm. Flatten and sprinkle a little sugar on it.',
		    '4.	Let it ripen, then lift and tidy up your creativity.'
		  ];
  	  	 }
  	  	 else if(this.id == 4)
  	  	 {
  	  	 	this.items = [
		    '(BOTTOM LAYER)',
		    '1.	Wash the soup and soak an hour (I soak for half an hour)',
		    '2.	Remove water and mix with coconut milk and salt.',
		    '3.	(Put a piece of pandan leaf to fray)',
		    '4.	Steam 20 minutes or until cooked.',
		    '5.	Press the cooked glutinous rice to make it compressible and beautiful.',
		    '6.	Draw a little bit of garbage to avoid scattered glutinous grease.',
		    '7.	Restore and pour the upper batter.',
		    '(UPPER LAYER)',
		    '1.	Put all the ingredients in the bowl and stir well. Filter.',
		    '2.	Pour slowly over the glaze.',
		    '3.	Spend about 30 minutes.',
		    '4.	Cool new pieces.'
		  ];
  	  	 }
  	  	 else if(this.id == 5)
  	  	 {
  	  	 	this.items = [
		    '1.	Cut a thin, thin banana with a fork until it crumbles. Do not be too crushed so that in the wrong, there is still a banana lump. Add sugar, brown sugar and salt. Stir well.',
		    '2.	Add flour and mix well until smooth.',
		    '3.	Cut long banana leaves 15cm x 15 cm and lay on the gas stove so that they cannot be torn when folded.',
		    '4.	Place 2 tablespoon batter on banana leaves. Also place grated coconut with a little salt. Fold left and right. Then fold it down, lift upright and fold it to the other end.',
		    '5.	Heat the steamer so that the water is boiling. Sort the wrong cakes in the steamer and steam for 20 minutes or until cooked.',
		    '6.	Apply the middle of the cake with a knife, if not sticky, the mark is cooked. Can be lifted and ready to be served.'
		  ];
  	  	 }
  	  	 else if(this.id == 6)
  	  	 {
  	  	 	this.items = [
		    '1.	Mix the bottom of the ingredients in the pan, mix well and cook until thick and shiny. Turn off the fire.',
		    '2.	Pour the ingredients into small bowls covered with 1/2 part of the bowl. Set aside.',
		    '3.	Cook the top layer until thick and pour over the bottom layer. Cool the room temperature.',
		    '4.	Pour as much sugar into the cake and serve. If you like to eat cold, keep a cake filled with sugar in the refrigerator.',
		    '5.	Sugar Melaka pandan leaves and water cooked until boiling and sugar dissolved. Filter and let it cool.'
		  ];
  	  	 }
  	  	 else if(this.id == 7)
  	  	 {
  	  	 	this.items = [
		    '1.	Add the red egg in a container and beat it until the flower. Dismiss.',
		    '2.	Then, unleash the rice flour with glutinous flour and fry without oil. Lift and cool.',
		    '3.	Cook sugar with two cups of water, pandan leaves and cloves of flowers.',
		    '4.	Prepare a bowl, put the beaten red egg and a little rice flour.',
		    '5.	Use your three fingers and bent like a buah tanjung. Repeat this step until it finishes',
		    '6.	Put in sugar water so that it absorbs, then it can be lifted.',
		    '7.	Ready to be served.'
		  ];
  	  	 }
  	  	 else if(this.id == 8)
  	  	 {
  	  	 	this.items = [
		    '1.	Heat the pan in the steamer first with a simple fire.',
		    '2.	Mix the ingredients in the blender and mix it until smooth. Then filter.',
		    '3.	Separate mix into 2 parts. Place a red dye on one side. Insert a little white mix in the second part',
		    '4.	Make a red layer in the container, leave it in 3 minutes.',
		    '5.	Enter the white mixtures anyway. Repeat these two steps until they run out.',
		    '6.	Once cooked, pick up and let cool new pieces.'
		  ];
  	  	 }
  	  	 else if(this.id == 9)
  	  	 {
  	  	 	this.items = [
		    '1.	Heat the sugar with water in a copper bowl for 20 minutes.',
		    '2.	Yellow egg beaten for 5 minutes.',
		    '3.	Take one egg yolks and put them in the sugar water (extinguish the fire).',
		    '4.	Place the pot over the fire for about 5 minutes, then lift.',
		    '5.	Press the three hot rods to keep warm as a three-sided shape'
		  ];
  	  	 }
  	  	 else if(this.id == 10)
  	  	 {
  	  	 	this.items = [
		    '1.	Mix glutinous rice with water. Enter the pandan leaf dye. Can also use pandan leaf water as a dye.',
		    '2.	Take the dough as big as the thumb and shape become round, then flip it in the centre.',
		    '3.	Rinse the water in a saucepan until it boils.',
		    '4.	Put the cake in a saucepan and remove it when the cakes arise. Evaporate and leave for a while.',
		    '5.	In a saucepan enter a cup of water and heat the sugar until melted. Place some pandan leaves.',
		    '6.	Add grated coconut and mix well.',
		    '7.	Finally, mix the kuih with the grated coconut.'
		  ];
  	  	 }
  	  	 else if(this.id == 11)
  	  	 {
  	  	 	this.items = [
		    '1.	The glutinous flour is mixed with pandan so that it forms a soft dough, and is left for a moment',
		    '2.	The grated coconut is cooked with red sugar and salt to the point.',
		    '3.	Construct the soft dough round and fill the FILLING inside it.',
		    '4.	Place a dough in the container and pour a thick coconut milk around it. Then steam until cooked.'
		  ];
  	  	 }
  	  	 else if(this.id == 12)
  	  	 {
  	  	 	this.items = [
		    '1.	Add the sifted wheat flour, coconut milk, egg, sugar & salt, yellow dye, pandan leaf water in a large bowl, mix well and filter.',
		    '2.	Cook in a pot over medium heat until thick and shiny.',
		    '3.	Remove and pour the dough into the tray, peel with banana leaves.',
		    '4.	Let it cool.',
		    '5.	Bake kuih using flower-shaped molds.',
		    '6.	Sprinkle fried onion over the cake.'
		  ];
  	  	 }
  	  	 else if(this.id == 13)
  	  	 {
  	  	 	this.items = [
		    'INSTRUCTION FOR KUIH',
		    '1.	Insert sugar in a cup filled 1/2 cup of warm water and bring in yeast. Leave for 10 minutes.',
		    '2.	Place the rice in a blender with 1 cup of water to be crushed, also input salt and rice flour. Blend until smooth.',
		    '3.	Remove from the blender jar and put in a container and pour the yeast mixture together with the sugar. Mix all over.',
		    '4.	Heat oil mold with a piece of pandan leaf dehydrated with oil. Let the oil in the hot mold completely hot. Close the kitchen and pour a dough full of full 3/4.',
		    '5.	Cook with slow ape until cooked.',
		    'INSTRUCTION FOR GRAVY',
		    '1.	Add sugar, water and eggs and beat well.',
		    '2.	Put the mixing ingredients into a medium heat cooker. Cook and stir this compound. When boiling, enter the mixture of coconut milk and corn flour.',
		    '3.	Cook until corn flour and coconut milk change colour to golden. The concentration is up to how long you cook.',
		    'INSTRUCTION FOR SERAWA DURIAN',
		    '1.	Mix all ingredients until boiling. Enter the egg whites later.',
		    '2.	Cook until gravy is slightly thick. '
		  ];
  	  	 }
  	  	 else if(this.id == 14)
  	  	 {
  	  	 	this.items = [
		    '1.	Fry the wheat flour until it comes out of a yellowish odour, frying and sifting into the container.',
		    '2.	Add water to a small saucepan and cook with melted sugar until the sugar is melted and spun.',
		    '3.	After that take a bamboo and dip in boiling sugar, lift and see if the sugar is nesting when exposed to air, so it is ready to be lifted.',
		    '4.	Reduce the fire, place one or two corn oil points into the copper pot. Sweep up flat.',
		    '5.	Then pour the dough mixture. Leave and leave until it appears frozen.',
		    '6.	Sweep the oil on two bamboo stems, lift it with two bamboo sticks, then pull the sugar to look golden.',
		    '7.	Heat the fried flour not to the heat.',
		    '8.	Then sift on oil paper. Take the pulled sugar and put it on the flour.',
		    '9.	Afterwards, pull the sugar over the flour and alternately sprinkle the wheat flour as the way to make bihun.',
		    '10. Pull the sugar so that the texture looks smooth and looks like bihun.',
		    '11. Store in airtight containers.'
		  ];
  	  	 }
  	  	 else if(this.id == 15)
  	  	 {
  	  	 	this.items = [
		    '1.	Mix all ingredients and stir on the fire until cooked and look a little shiny.',
		    '2.	Let cool.',
		    '3.	Round or can be shaped according to their own preferences.'
		  ];
  	  	 }
  	  	 else if(this.id == 16)
  	  	 {
  	  	 	this.items = [
		    '1.	The Melaka sugar is cooked with three glasses of water to break. One pack of rice flour is brewed with five glasses of water.',
		    '2.	Then add a little chalk water.',
		    '3.	Pour the sweets into the flour mix, stir until well mixed and put in a little salt.',
		    '4.	Banana leaves are made of clasps and are erected. Add the dough together with sweets.',
		    '5.	The 3/4 budget is full of the cladding. Then steam until cooked.',
		    '6.	When cooking, put the white layer over the claw so full and steamed until cooked. '
		  ];
  	  	 }
  	  	 else if(this.id == 17)
  	  	 {
  	  	 	this.items = [
		    '1.	Grate the cassava and boil water until there is no cassava. Then put in water and a little salt and mix well in a pan and press to put the tubers in the pan and then steam.',
		    '2.	Cook the gravy sugar (not too thick) and remove the young coconut grate and put in a little salt. Fried glutinous rice until golden brown.',
		    '3.	Once everything is cooked and ready. Cut a small casserole and mix well in young coconut and sprinkle it on top. Then pour the gravy sugar on top.'
		  ];
  	  	 }
  	  	 else if(this.id == 18)
  	  	 {
  	  	 	this.items = [
		    '1.	Add 100 ml of water into a saucepan with brown sugar and white sugar as well as a pandan leaf.',
		    '2.	When sugar water has boiled, add 225 ml of cold water and stir well until it is warm, let it stand for a while.',
		    '3.	Put wheat flour + rice flour and salt in a large bowl and filter the sugar into it and stir well.',
		    '4.	Put in closed containers and leave overnight.',
		    '5.	Heat 2/3 oil in a small pan.',
		    '6.	Stir in the dough that has been left over, put the dough in a small bowl and pour in middle of the oil to form a round cake.',
		    '7.	Let it cook on the edges and the middle. Pick up and drain'
		  ];
  	  	 }
  	  	 else if(this.id == 19)
  	  	 {
  	  	 	this.items = [
		    '1.	Combine coconuts, glutinous flour and wheat flour in containers, add salt, mix well. Add water gradually, turning to a soft dough. Construct the dough according to taste.',
		    '2.	Fry with a simple fire.',
		    '3.	Water and sugar are cooked in a pan, so just add in slowly kuih getas. Stir until dry.'
		  ];
  	  	 }
  	  	 else if(this.id == 20)
  	  	 {
  	  	 	this.items = [
		    '1.	Cook 1 cup of coconut milk until it becomes a dung of oil, with a slow fire. When ready, take the resulting oil shale and leave it in 2 tablespoons of extra oil in the pan.',
		    '2.	Cook in brown sugar, sugar, water and pandan leaves until sugar is dissolved, cool and set aside for a while.',
		    '3.	Combine green beans, coconut milk, salt and brown sugar in a container and stir well.',
		    '4.	Put the dough into a pan containing the excess coconut oil. Its always in the mess to be unobtrusive later. Cook until thick and bubbly until slightly shiny. Pour the dough into a brass that has been swept with the excess oil.',
		    '5.	Sprinkle with oil shutoff and cool before it can be cut.'
		  ];
  	  	 }
  	  	 else if(this.id == 21)
  	  	 {
  	  	 	this.items = [
		    '1.	Sift wheat flour and yam flour together with baking powder.',
		    '2.	Beat eggs and sugar until flowers and add vanilla essence.',
		    '3.	Mix the sifted flour with egg dough. Stir well',
		    '4.	Preheat the oven to 250 degrees',
		    '5.	Wipe the mold with oil (to prevent it from sticking) and place the batter in the mold.',
		    '6.	Bake for about five minutes.'
		  ];
  	  	 }
  	  	 else if(this.id == 22)
  	  	 {
  	  	 	this.items = [
		    '1.	Flour and flour in sifted flour along with baking powder.',
		    '2.	Then mix well.',
		    '3.	Add coconut milk, eggs and sugar and beat them until the flowers.',
		    '4.	Put the flour into the dough mixture gradually until it becomes a dough.',
		    '5.	Apply with the mold.',
		    '6.	Bake with moderate temperature until cooked.'
		  ];
  	  	 }
  	  	 else if(this.id == 23)
  	  	 {
  	  	 	this.items = [
		    '1.	Cook the red sugar with water until it boils and filters.',
		    '2.	Pour little by little hot sugar into the rice flour already in the container.',
		    '3.	Stir in the spoons until smooth. If the dough appears to be greasy, add rice flour',
		    '4.	Before we start to fry, the mixture should be dug first with some water (to soften it).',
		    '5.	Then take a little bit then round it like a marble. Press with fingers to get round and flat shape.',
		    '6.	Build according to creativity',
		    '7.	Once formed, continue to fry in hot oil until dry (use hot heat & fry not too long).',
		    '8.	Lift & pour oil.'
		  ];
  	  	 }
  	  	 else if(this.id == 24)
  	  	 {
  	  	 	this.items = [
		    '1.	Mix all the ingredients in the pan.',
		    '2.	Combine with a little water, then gobble over medium heat.',
		    '3.	Cover the FILLING until it is tough and almost dry. Then it can be lifted and set aside.',
		    '4.	To prepare the flour material, the glutinous flour should be mixed with raw coconut milk mixed with some salt.',
		    '5.	Spin the dough until it becomes a soft dough and can be formed. Then, divide the dough into medium-sized parts.',
		    '6.	Break the dough and fill the FILLING in the middle.',
		    '7.	Then round and roll into cooked coconut milk.',
		    '8.	After that wrap the dough.',
		    '9.	Make it done.',
		    '10.	When ready, prepare the steamer and steaming the dough until cooked.'
		  ];
  	  	 }
  	  	 else if(this.id == 25)
  	  	 {
  	  	 	this.items = [
		    '1.	Break the eggs into a mixing bowl. Add in sugar followed by the coconut milk and whisk until the sugar gets dissolved.',
		    '2.	Sift together the rice flour, the all-purpose flour and the salt. Add this flour mixture to the mixing bowl in (1). Whisk until they combine really well.',
		    '3.	Heat up enough cooking oil in a wok on high heat. Make sure that the oil is well heated before you begin. I have it tested with a wooden chopstick. A stream of tiny bubbles seen as the end of the chopstick is dipped into the oil indicates that the oil is ready. Turn the heat down to medium. Place the brass mould into wok and let sit and preheat for about a minute.',
		    '4.	Transfer the mould into the batter and dip just deep enough to cover the sides neatly, leaving the top surface batter-free. Keep still for about 5 seconds.',
		    '5.	Transfer the mould into the hot oil, keeping it immersed for about 15 seconds. Lightly shake to free the cookie and leave the mould in the hot oil.',
		    '6.	Let the first cookie fry while you bring in the second cookie repeating steps (4) - (5) ',
		    '7.	When the first cookie has turned golden brown, remove the cookie and drain on a sieve. ',
		    '8.	Repeat steps (4) - (7) until all the batter has been used up.',
		    '9.	Let cool completely and store in air-tight containers'
		  ];
  	  	 }
  	  	 else if(this.id == 26)
  	  	 {
  	  	 	this.items = [
		    'INSTRUCTIONS TO MAKE DOUGH',
		    '1.	Boil the tubers until soft and drained.',
		    '2.	When the tubers are cold, finely pounded with a mortar and pull out the rough veins of sweet potatoes.',
		    '3.	It can also use a potato masher to crush the sweet potatoes.',
		    '4.	Add the flour gradually until the dough is less sticky.',
		    '5.	Pluck the dough ball and make a hole in the middle.',
		    '6.	Repeat the process until it is over and fry in hot oil until it is fried.',
		    '7.	Stir well or fry the fried cakes in deep boiling sugar that is boiling evenly.',
		    'INSTRUCTIONS FOR SUGAR COATING',
		    '1.	Pour finely sugar and cook in a pan to boil and concentrate.',
		    '2.	Ready to mix culinary cookies'
		  ];
  	  	 }
  	  	 else if(this.id == 27)
  	  	 {
  	  	 	this.items = [
		    'INSTRUCTIONS TO MAKE GLUTINOUS RICE LAYER',
		    '1.	Clean and drip the glutinous rice so that the water is dry.',
		    '2.	Insert into the Brass (10 × 10) covered with plastic paper (14 × 14)',
		    '3.	Apply oil around the pan.',
		    '4.	Add coconut milk and salt to the pan.',
		    '5.	Then pour the mixture into the glutinous rice.',
		    '6.	Mix well and steam for 25 minutes. Occasionally be stirred to make glutinous rice cooked more smoothly.',
		    '7.	Sweetener should have the same level.',
		    '8.	Sprinkle rice with all ingredients for 30 minutes.',
		    '9.	After cooking, press it until it is compressed and solid in the pan.',
		    '10.Ready to overlay with top layer.',
		    'INSTRUCTIONS TO MAKE UPPER LAYERS',
		    '1.	Put the wheat flour, corn flour, eggs, concentrated coconut milk, sugar and salt into the container. Stir well. Then add water and stir again.',
		    '2.	After that, add a green dye and a little pandan leaf.',
		    '3.	Combine all the ingredients and stir until smooth and uniform with whisker.',
		    '4.	Filter or mix to make sure there are no buns in the dough flour.',
		    '5.	Then pour the mixture (green dough) over the pressed glutinous rice.',
		    '6.	Steam for 1/2 hour or until cooked.',
		    '7.	Cover the pan cover with aluminium foil, towel or clean cloth so that the steam water does not drip on the surface of the cake.',
		    '8.	Cook for a while on the fire slowly until it gets steam.',
		    '9.	Every 10 minutes the lid is opened to remove the water vapour.',
		    '10.	Cool it thoroughly before removing it from the hollow mold.',
		    '11.	After that, cut into pieces according to size to taste and served.'
		  ];
  	  	 }
  	  	 else if(this.id == 28)
  	  	 {
  	  	 	this.items = [
		    '1.	Blend 1/3 grated coconuts with pandan leaves, with water estimates to produce 2 cups of coconut milk.',
		    '2.	Dairy and filter.',
		    '3.	Mix coconut milk with wheat flour, salt, eggs and some dots if desired, so that the skin becomes greener.',
		    '4.	Stir well then use whisk and then strain so there is no lump. Fluorination should be slightly liquid.',
		    '5.	Heat the frying pan with some oil.',
		    '6.	Pour one spoon of flour mix and flatten it by turning the pan to the left and right to get a round shape.',
		    '7.	Align the top using a spat if necessary.',
		    '8.	Grad 3-4 minutes on the pan.',
		    '9.	Raise and roll with the cooled filling.'
		  ];
  	  	 }
  	  	 else if(this.id == 29)
  	  	 {
  	  	 	this.items = [
		    '1.	Put all the ingredients in the blender and blend until smooth. Im just a whisk.',
		    '2.	Apply baking sheet with a little oil and put in an oven before pouring the dough into it.',
		    '3.	Sprinkle sesame. I sprinkle sesame seeds after baking in 10-15 minutes.',
		    '4.	Bake 40 minutes at 180C until cooked or adds to baking time in another 20 minutes to get crusty / dry surfaces. I burned almost 1 hour and a half.'
		  ];
  	  	 }
  	  	 else if(this.id == 30)
  	  	 {
  	  	 	this.items = [
		    '1. Heat steamer',
		    '2. Beat eggs and sugar until they are blossomed. Press ovulate and hit again.',
		    '3. Add coconut milk, flour and green dye. Stir well again.',
		    '4. Sweep a little oil on the mold.',
		    '5. Fill the coconuts at the bottom of the mold and press for compress.',
		    '6. Put the dough and steamed for about 7 minutes.'
		  ];
  	  	 }
  	  	 else if(this.id == 31)
  	  	 {
  	  	 	this.items = [
		    '1.	Take medium-sized pandan and mix in a blender with a 1/2 cup of water. Then filter',
		    '2.	Mix pandan and coconut milk',
		    '3.	Mix it with sugar, corn flour, a pinch of salt, and a little green dye if the pandan green colour is less prominent.',
		    '4.	Stir until smooth and not tight',
		    '5.	Then cook with medium heat until it is viscous',
		    '6.	Combine the ingredients A and the ingredients of B',
		    '7.	Stir well until it becomes thick and smooth',
		    '8.	Set aside first while we prepare the White dough',
		    '9.	Combine all the ingredients C, the richness of ripe on medium heat while constantly stirring until it is thick',
		    '10.	Wait for warm enough to put in piping bag',
		    '11.	In the meantime we heat up the steam until the water is boiling',
		    '12.	Cover the frying lid with cloth so that the water on the steamer lid does not drip on the kuih.',
		    '13.	Prepare a small hollow mold, apply hollow mold with cooking oil',
		    '14.	Pour the green dough into the hollow mold until 3/4 cup of hollow mold',
		    '15.	Then put the white dough in the center of the green dough until the dough rises with a hollow cup',
		    '16.	Arrange in steamer and steam for 10 minutes',
		    '17.	Pick up and let cool'
		  ];
  	  	 }
  	  	 else if(this.id == 32)
  	  	 {
  	  	 	this.items = [
		    '1.	Mix the glutinous flour and rice flour with water of pandan leaves and lime water gradually until the dough can be stirred and not attached to the hand.',
		    '2.	Take a dough of thumb and flip it. Add Melaka sugar which is cut or crushed so that it is properly melted when boiled. I do not like the flavour of Malacca sugar when it was chewed. Close the dough mix and dough mix with both hands to make a beautiful round shape.',
		    '3.	Heat the water in a saucepan until it boils. If you like, you can add Panda leaves in boiling water to perfumes.',
		    '4.	Insert the flour balls and wait until they appear on the surface of the water indicating they are cooked and ready to be drained out.',
		    '5.	Store and continue to put into grated coconut plates that have been mixed with some salt. Find a cake on the coconuts until they are properly coated. '
		  ];
  	  	 }
  	  	 else if(this.id == 33)
  	  	 {
  	  	 	this.items = [
		    '1.	Mix all the ingredients for the bottom layer and stir until it is clear to concentrate on a simple fire.',
		    '2.	Insert green flavour. Stir well.',
		    '3.	Put in a pandan leaf that is formed like a full half bowl.',
		    '4.	Store the top layer mixture and stir until thick on a simple fire.',
		    '5.	Pour over the bottom layer until full',
		    '6.	Steam for 3-5 minutes.'
		  ];
  	  	 }
  	  	 else if(this.id == 34)
  	  	 {
  	  	 	this.items = [
		    '1.	Make the syrup: put the dark palm sugar, knotted pandan leaf and water into a pot and cook until it becomes a syrup. Then add regular sugar and stir until theyve all dissolved. Remove the pandan leaf and let cool.',
		    '2.	Prepare egg cups or mold in a steamer and lightly steam. Mix the rice flour and tapioca flour together and then add the alkaline water. Pour in the cooled syrup and stir to create a smooth batter.',
		    '3.	Cook the batter in a pan over very low heat, stirring continuously until it thickens slightly and immediately remove from heat.',
		    '4.	Pour the batter into the egg cups about ¾ full. Steam for about 15 minutes or until its cooked and set. Remove from heat and let cool completely before unmoulding.',
		    '5.	Mix the grated coconut with a little salt. Unmould the little cakes and top with coconut.'
		  ];
  	  	 }
  	  	 else if(this.id == 35)
  	  	 {
  	  	 	this.items = [
		    '1.	Heat about 1 tbsp. of oil and add to this your onion and garlic sauce until soft.',
		    '2.	Add in the curry powder and just a bit of water to make a paste, fry until the curry is aromatic.',
		    '3.	To this add the minced beef and season with salt and sugar.',
		    '4.	Take off the heat, set aside.',
		    '5.	Make the batter by adding all the ingredients into a bowl. Make sure the consistency is of pancake batter not heavy but light.',
		    '6.	Now heat up takoyaki pan if you have one, if not improvise. You can pour the batter on a flat pan and make them into tiny pancakes. Make sure your heat is at medium and not high or else you will burn the batter.',
		    '7.	When the batter is ¼ set, spoon in the ready-made filling and just wait till it sets. Ready to serve and eat immediately.'
		  ];
  	  	 }
  	  	 else if(this.id == 36)
  	  	 {
  	  	 	this.items = [
		    '1.	In a blender, blend both pandan leaves and 2 cups water until smooth.  Pass through a fine sieve into a small bowl.',
		    '2.	In a mixing bowl, add plain flour and salt.  Pour pandan leaves juice a little at a time while keep stirring.  Then, add coconut milk, castor sugar and eggs (one at a time). Mix thoroughly and then, add a few drops of yellow colouring (optional).  Pass mixture through a fine sieve into another bowl to get rid of any lumps.',
		    '3.	After "seasoning" the mould and removing the oil, heat mould over an electric stove. Grease mould with ghee by using a small brush.',
		    '4.	Put on the lid and cook over high heat for about 5 minutes or until cooked.',
		    '5.	Once done, remove the kuih by using a toothpick and a tablespoon.  Leave to cool down on a tray before putting two halves together.  Serve either warm or cold.'
		  ];
  	  	 }
  	  	 else if(this.id == 37)
  	  	 {
  	  	 	this.items = [
		    'INSTRUCTION FOR (A)',
		    '1. Cook the sugar over the small fire until the new war enters the water and the butter let it cool / warm',
		    'INSTRUCTION FOR (B)',
		    '1. Bowl of eggs at an egg with no sugar is not what. Add another ingredient and mix ingredients once.',
		    '2. Enter in a small bowl steaming in a bowl.',
		    '3. Make sure the steam is freshly steamed within 15 minutes (depending on which mold is used)',
		    '4. If sticky, place a small plant on the mold.'
		  ];
  	  	 }
  	  	 else if(this.id == 38)
  	  	 {
  	  	 	this.items = [
		    'INSTRUCTION TO MAKE A DOUGH',
		    '1.	Mix glutinous flour, wheat flour and salt.',
		    '2.	Mix with a little bit of water until it becomes a dough that does not stick in the hands.',
		    '3.	If lightened, add half a tablespoon of glutinous rice flour every time you enter it.',
		    'INSTRUCTIONS TO MAKE CORE',
		    '1.	Boil the water in a pan. Add red sugar, salt and Pandan leaves.',
		    '2.	Cook until sugar dissolves. Add grated coconut. ',
		    '3.	Stir occasionally until the water is dry. ',
		    '4.	When the FILLING is slightly damp, pour the wheat flour. ',
		    '5.	Stir well. (The flour function is so that the FILLING sticks and does not collapse). ',
		    '6.	Cool the core.',
		    'INSTRUCTION TO MAKE KUIH BOM',
		    '1.	Sweep a little oil on the palm of your hand. ',
		    '2.	Take the batter, platter and put the FILLING that has been rounded.',
		    '3.	Include the flat part of the tip to the end. Round it neatly.',
		    '4.	Continue to digging into the sesame paste until all the cakes are smeared with sesame seeds.',
		    '5.	Lastly, put between 2 palms for the non-sticky sesame to fall on its own.',
		    '6.	Fry the bombs in the oil soaked with a moderate fire at 5 minutes. ',
		    '7.	Then just flush the flame while stirring it rolled in a pan until it was quite yellowish. Not long after, explode.'
		  ];
  	  	 }
  	  	 else if(this.id == 39)
  	  	 {
  	  	 	this.items = [
		    'INSTRUCTION FOR KUIH LOPES',
		    '1.	Wash glutinous rice, soak with glutinous rice with water, green dye and chalk water for 1/2 hour. Meanwhile, pandan leaves together with 1/2 cup of water and filter the water. Set aside first.',
		    '2.	Then pour the glutinous rice into a baking sheet. ',
		    '3.	Then, add coconut milk, salt and Pandan.',
		    '4.	Mix smoothly. Preheat the steamer until the water is boiling and steamed until cooked or 3/4 is cooked no matter what cause will be steamed again.',
		    '5.	When the coconut milk is dry, stir well to make the average glutinous rice cooked. Steam immediately before lifting. Cool for a moment.',
		    '6.	Arrange on steamer and steam again for 15 minutes or more. Cool and cut using stretched thread on either side or use a knife only. ',
		    '7.	Mix with grated coconut that has been mixed with salt.',
		    '8.	Take a banana leaf that is cut long and crush a minute over the fire. ',
		    '9.	Lay the banana leaves, stick with the desired amount and roll the leaves in two or three layers. Scroll left and right while compacting. ',
		    '10.Tie with yarn or ropes.',
		    'INSTRUCTION FOR GULA MELAKA',
		    '1.	Put all the ingredients in the pan, when boiling lightly and simmer until cooked. ',
		    '2.	When its cold it can be eaten with glutinous rice.'
		  ];
  	  	 }
  	  	 else if(this.id == 40)
  	  	 {
  	  	 	this.items = [
		    '1.	Boil tubers until tender. Scrub.',
		    '2.	Cook the tubers and mix with wheat flour. It is quite a tendency to mix it. If it is over wheat, the dough becomes hard.',
		    '3.	Bake cake checks. Add a spoonful of sugar and form longitudinal.',
		    '4.	Fry in a lot of hot oil with a slow fire so that sugar can break into sugar when fried.',
		    '5.	Remove when yellow. Watch the hot sugar, so be cautious if you eat a tasty check in hot heat.'
		  ];
  	  	 }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InstructionPage');
  }

    dismiss() {
    this.viewCtrl.dismiss();
  }


}
