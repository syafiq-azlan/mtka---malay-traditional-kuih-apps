import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { IngredientPage } from '../ingredient/ingredient';
import { InstructionPage } from '../instruction/instruction';

/**
 * Generated class for the DetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail',
  templateUrl: 'detail.html',
})
export class DetailPage {
	public id;
	public detail;
	public image;
	public name;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController) {
  	  this.id = navParams.get("id");

  	  if(this.id == 1)
  	  {
  	  	this.image = "assets/img/kuih1.jpg";
  	  	this.name = "Agar Agar Kering";
  	  	this.detail = "Agar-agar kering or also known as agar beleda in Kelantan and Terengganu is a kind of old traditional food. It is delicious, sweet and durable.Agar-agar kering is essentially made of sweet jelly that has been dried. Agar-agar kering is usually made of various colours to attract and dry the semi-dry so it is not easily damaged and durable. Basically the agar is made as usual, but with the addition of rock sugar instead of granulated sugar. More than one colour is produced and pandan leaves are used to create a scent. Then when the agar has become hard, the agar will be cut at the appropriate cut size. The size of the cut will usually decrease after drying when the gel is lost and turns into dried agar. Agar-agar kering usually have a look of glass, due to the hardened sugar content surrounded it. It has a crisp taste in the outer and soft interior. Dried agar are usually rarely produced and made only for special events such as Hari Raya Aidilfitri and Hari Raya Aidil Adha.";
  	  }
  	  else if(this.id == 2)
  	  {
  	  	this.image = "assets/img/kuih2.jpg";
  	  	this.name = "Halwa Maskat";
  	  	this.detail = "Halwa Maskat is a kind of traditional sweet dish served only on festive days. It is from Penang. Halwa Maskat is a kind of kuih shaped like rectangular shaped like jelly that is produced from beef, flour, and sugar. The secret of producing quality is not merely depending on the correct ingredient, but also on its complicated production methods and takes time from preparation to continuous stirring. The mix to produce halwa maskat should be fermented for three days three nights before being stirred on a slow fire for five hours without stopping. If not, the mixture may be crusty and warm.";
  	  }
  	  else if(this.id == 3)
  	  {
  	  	this.image = "assets/img/kuih3.jpg";
  	  	this.name = "Kuih Dangai";
  	  	this.detail = "Kuih dangai are very popular in the north, this cake is made of coconut, sugar and glutinous flour. This is similar to the beko pulut on the east coast, the difference is not to use eggs and beko cookies using coconuts and flour.";
  	  }
  	  else if(this.id == 4)
  	  {
  	  	this.image = "assets/img/kuih4.jpg";
  	  	this.name = "Kuih Tok Aji Serban ";
  	  	this.detail = "A kind of cake cooked with glutinous rice under a sweet coconut. In addition, the kuih Tok Aji Serban almost resembles a Kuih Seri Muka but the coconut blend is usually brown and not green.";
  	  }
  	  else if(this.id == 5)
  	  {
  	  	this.image = "assets/img/kuih5.jpg";
  	  	this.name = "Kuih Lepat Pisang";
  	  	this.detail = "Kuih lepat or also known as kuih belebat refers to a kuih group produced by wrapping it in a banana leaf and steamed. There are various types of cakes that are available, depending on the ingredients used.";
  	  }
  	  else if(this.id == 6)
  	  {
  	  	this.image = "assets/img/kuih6.jpg";
  	  	this.name = "Kuih Lompat Tikam";
  	  	this.detail = "Kuih Lompat Tikam is a popular traditional kuih in Malaysia, especially in the East Coast in Pahang, Kelantan and Terengganu. It is made of rice flour and coconut milk. Kuih Lompat Tikam have two layers, namely a layer of flour that is usually colour green on the bottom and a layer of coconut milk white in the top. This part is tasteless because it is eaten with gravestones or coconut sugar. The History of Kuih Lompat Tikam was originally from the kingdom of Mahapahit to be presented to the Government of Kelantan around the year 1700. When the kuih was about to be presented, the maid of palace was very careful when carrying these trays, but one of the maid was stuck in the knapsack at the palace, it make Sultan very shocked, and Sultan stabbed the maid . In conjunction with the story, the Kelantan officially named it as Kuih Lompat Tikam.";
  	  }
  	  else if(this.id == 7)
  	  {
  	  	this.image = "assets/img/kuih7.jpg";
  	  	this.name = "Kuih Buah Tanjung";
  	  	this.detail = "Kuih Buah Tanjung are a kind of traditional Malay cakes. This kuih is popular in Kelantan and is a special meal there. It is a kind of kuih made using egg mixed with flour and mixed in sugar water. Kuih Buah Tanjung are shaped like drops of water and yellow. It is moulded using molds and shaped according to the taste of the maker in sugar water. It is more for special cakes for special occasions such as for a wedding party.";
  	  }
  	  else if(this.id == 8)
  	  {
  	  	this.image = "assets/img/kuih8.jpg";
  	  	this.name = "Kuih Lapis";
  	  	this.detail = "Kuih lapis is a kind of kuih that is found in Malaysia. This kuih consists of several layers and can be found in various colours, but red is a traditional colour. In the past, the perfect layer for this kuih is at least 18 layers. The time to make this kuih take three or four hours. Kuih lapis is introduced from the Netherlands. Those who brought the legit, but when they arrived in Indonesia, the people tried to apply with the spice richness, which eventually became the legit cakes of the Indonesian version.";
  	  }
  	  else if(this.id == 9)
  	  {
  	  	this.image = "assets/img/kuih9.JPG";
  	  	this.name = "Mas Sejemput";
  	  	this.detail = "Mas Sejemput is a kind of sweet-cooked Malay curd that is produced by using egg yolks cooked in sugar water, just like mas. In addition, the pick-up is provided by using the beaten egg yolk so that it rises slightly and is diced into boiling sugar water.";
  	  }
  	  else if(this.id == 10)
  	  {
  	  	this.image = "assets/img/kuih10.jpg";
  	  	this.name = "Puteri Mandi";
  	  	this.detail = "A popular dish of Kelantan. Kuih puteri mandi have a variety of colours, red, green, yellow and white. Besides, it has a sweet taste and fat.";
  	  }
  	  else if(this.id == 11)
  	  {
  	  	this.image = "assets/img/kuih11.jpg";
  	  	this.name = "Kuih Ocong-Ocong";
  	  	this.detail = "Known as yongkong is a popular traditional cake in Malaysia. The ocong-ocong kuih are made from glutinous flour, shaped like a FILLING with inner FILLING and cooked with a thickened coconut milk sauce that gives sweet fat.";
  	  }
  	  else if(this.id == 12)
  	  {
  	  	this.image = "assets/img/kuih12.jpg";
  	  	this.name = "Kuih Qasidah";
  	  	this.detail = "Kuih Qasidah or also called Kasidah cake is a kind of sweet and sweet tradition. It is produced using a mixture of flour and beef and sugar. Usually the cake is rounded and placed on top of the tray, before it is sticky, using a clasp forming like a rose. After that, fried onions will be sown on top.";
  	  }
  	  else if(this.id == 13)
  	  {
  	  	this.image = "assets/img/kuih13.jpg";
  	  	this.name = "Kuih Serabai ";
  	  	this.detail = "Kuih Serabai is a traditional kuih which is popular in Malaysia .It made of rice flour, and eaten with Melaka and coconut milk syrup flavoured sweet sugars. It is popular in Kedah, Melaka, and Sabah.";
  	  }
  	  else if(this.id == 14)
  	  {
  	  	this.image = "assets/img/kuih14.jpg";
  	  	this.name = "Slemang";
  	  	this.detail = "Slemang is a traditional food of the East Coast community originating from the state of Terengganu. It is a decomposing sugar that is brewed in flour. Has a sweet taste. Slemang is the name for his creator, Sulaiman.";
  	  }
  	  else if(this.id == 15)
  	  {
  	  	this.image = "assets/img/kuih15.jpg";
  	  	this.name = "Tahi Itik";
  	  	this.detail = "Tahi itik is a popular cake in the east coast like Kelantan and Terengganu. It is a kind of cake made with eggs and hot in sugar. It was formed as a duck.";
  	  }
  	  else if(this.id == 16)
  	  {
  	  	this.image = "assets/img/kuih16.jpg";
  	  	this.name = "Kuih Tepung Pasung";
  	  	this.detail = "Chicken flour is a favourite cakes on the east coast. It is good to eat in the morning or evening. Flour is made of rice flour and has a FILLING. It is placed in a cone mold made from a banana leaf.";
  	  }
  	  else if(this.id == 17)
  	  {
  	  	this.image = "assets/img/kuih17.jpg";
  	  	this.name = "Kuih Puli Ubi Kayu";
  	  	this.detail = "Kuih Puli Ubi Kayu is a popular cake in the east coast like Kelantan and Terengganu. It is a kind of cake made with cassava that is bitten and wrapped in banana leaves before boiled or steamed. It is then eaten with the sweetened sugar made from Melaka sugar.";
  	  }
  	  else if(this.id == 18)
  	  {
  	  	this.image = "assets/img/kuih18.jpg";
  	  	this.name = "Kuih Penyaram ";
  	  	this.detail = "The kuih penyaram or known as the pinjaram is a kind of traditional cake in Sabah and Brunei. It is also popular among the people. It is made of rice flour, corn flour, coconut milk, and oil. Pandan leaves are used to add fragrant scents.";
  	  }
  	  else if(this.id == 19)
  	  {
  	  	this.image = "assets/img/kuih19.JPG";
  	  	this.name = "Kuih Getas";
  	  	this.detail = "Kuih getas or getas-getas is a kind of traditional cake flavoured with sugar water. It is also known as Kemplang kuih. It is usually made from oval-shaped rice flour and after blended it fried in sugar. It can be eaten as a snack anytime and its soft and chewy flavour makes it extremely popular.";
  	  }
  	  else if(this.id == 20)
  	  {
  	  	this.image = "assets/img/kuih20.jpg";
  	  	this.name = "Kuih Kole Kacang";
  	  	this.detail = "Kole kacang originated from Malaysia and it was said that Kole kacang was from Kelantan, which the word Kole itself originated from the Kelantanese word for kuih, which is kole. Kole kacang is considered national heritage as it is considered rare and not many citizens are capable in making it.";
  	  }
  	  else if(this.id == 21)
  	  {
  	  	this.image = "assets/img/kuih21.jpg";
  	  	this.name = "Kuih Bahulu";
  	  	this.detail = "Kuih Bahulu is made from a mixture of eggs, wheat flour and sugar.  It is a traditional cake of the Malays since the olden days.  In the past, the creation of kuih bahulu comprises of a long process of mixture preparation and process of baking. Kuih Bahulu’ has become the foremost item in the menu of festive foods for the Malay society on festivals and important ceremonies such as Hari Raya and weddings.  The ability to prepare beautiful and delicious ‘Kuih Bahulu’ is the pride of every baker as it will be savoured by lovers of Kuih Bahulu.  As such, our family bears the proud heritage of ‘Kuih Bahulu’ bakers that are very much sought after during festive seasons and public functions.";
  	  }
  	  else if(this.id == 22)
  	  {
  	  	this.image = "assets/img/kuih22.jpg";
  	  	this.name = "Kuih Bangkit ";
  	  	this.detail = "Kuih Bangkit is a kind of traditional kuih made from wheat flour and sago flour. Kuih Bangkit can be dissolved in the mouth and have a crisp taste if chewed. It's sweet to interest children.";
  	  }
  	  else if(this.id == 23)
  	  {
  	  	this.image = "assets/img/kuih23.jpg";
  	  	this.name = "Kuih Denderam";
  	  	this.detail = "Kuih denderam or also known as cakes or ruffles on the east coast of Peninsular Malaysia is a delicious, sweet pastry. In recent years, the word ‘keling’ has not been adopted for fear of racial issues. According to an article from The Star newspaper, this curd is inspired by the southern Indian cakes but has become popular among Malaysians. In India it is known as Adhirasam and uses black pepper as one of its ingredients.";
  	  }
  	  else if(this.id == 24)
  	  {
  	  	this.image = "assets/img/kuih24.jpg";
  	  	this.name = "Kuih Koci";
  	  	this.detail = "Kuih koci is a popular traditional cake in Malaysia, Indonesia, and Singapore. This cake is made from glutinous flour and has a sweet coconut FILLING mixed with the sugar glaze (Melaka Sugar) in it. In Malaysia it is popular especially in the East Coast area in Kelantan and Terengganu. It is then coated with coconut milk before wrapping with banana leaves and served.";
  	  }
  	  else if(this.id == 25)
  	  {
  	  	this.image = "assets/img/kuih25.jpg";
  	  	this.name = "Kuih Ros";
  	  	this.detail = "Kuih Ros is a tradition in Kedah. It is also known as Kuih Loyang, Kuih Bunga Durian, Kuih Cap and Kuih Goyang. The food is made from rice flour, coconut milk, sugar, salt, eggs, lime, cooking oil and water. The community in the Padang Terap area of Kedah named this dish as a Kuih Goyang because after its mold dipped into a liquid flour mixture, it was dipped into a hot oil and shaken to date.";
  	  }
  	  else if(this.id == 26)
  	  {
  	  	this.image = "assets/img/kuih26.JPG";
  	  	this.name = "Kuih Keria";
  	  	this.detail = "Kek keria (or kuih gelang) is a traditional Malay cakes. Based on sweetened flour with a mixture of flour, sugar and salt. It is polished and shaped like a ring or a bracelet. Usually, this cake is covered with sugar or sugar (liquid sugar).These kuih are often flavoured and served during breakfast and evening tea.";
  	  }
  	  else if(this.id == 27)
  	  {
  	  	this.image = "assets/img/kuih27.jpg";
  	  	this.name = "Kuih Seri Muka";
  	  	this.detail = "Kuih Seri Muka is a kind of Malaysian traditional kuih. Kuih Seri Muka is usually made in a large bowl that is round or square or rectangular. Once the kuih is cooked, then it is cut according to the size of the dish in the desired form. Kuih Seri Muka are usually produced in two layers and traditionally in green (top layer) and white (bottom layer). This is because in ancient times pandan leaves are used as dye and are more readily available. There are currently several colours of food used to attract buyers. Kuih Seri Muka is made of glutinous rice, while the top layer is based on eggs and coconut milk and pandan leafs.";
  	  }
  	  else if(this.id == 28)
  	  {
  	  	this.image = "assets/img/kuih28.JPG";
  	  	this.name = "Kuih Ketayap";
  	  	this.detail = "Kuih Ketayap or also known as kuih gulung are a popular cakes tradition in Malaysia. It is a kind of kuih consisting of round thin skin and placed in the FILLING made of young coconut and Malacca sugar in it. Kuih ketayap are probably due to how to make them where the outer shells are rolled to the FILLING. The other name of the cake is also due to the appearance of the outer shell like a crawfish before being rolled together with the inner FILLING. These tires are popular throughout Malaysia. It's made a lot, and is sold everywhere, and does not need to be booked in advance. Kuih ketayap is popular among young people as daily food because it is a kind of delicious kuih. Usually the plastic layer is used to stick a sticky outer skin to avoid exposure to the finger of the person eating it.";
  	  }
  	  else if(this.id == 29)
  	  {
  	  	this.image = "assets/img/kuih29.jpg";
  	  	this.name = "Kuih Bakar";
  	  	this.detail = "Kuih Bakar are one of the traditional Malay kuih. This kuih is also known as bahulu kemboja. This kuih is round and sprinkled with sesame seeds on it. It is very good, delicious and delicious as a breakfast, evening or dessert for official occasions, weddings, and festivals and so on. Among the ingredients used to make baked cereals are wheat flour, starch, eggs, beef, sesame and flavouring and enough dye. How to make this kuih is using a special mold and it is burned with a charcoal stove.";
  	  }
  	  else if(this.id == 30)
  	  {
  	  	this.image = "assets/img/kuih30.jpg";
  	  	this.name = "Kuih Puteri Ayu";
  	  	this.detail = "Puteri Ayu is also known as Kuih Nyonya.It is an Indonesian kuih which is green in colour with white coconut topping.It’s a delicious kuih,fragrant with pandan and with a lovely light,moist texture like sponge cake. This kuih is normally steamed in small plastic moulds which you can get from the night market.Buy as many moulds as you can fill in your steamer so that you steam many kuihs in one time.The kuih comes in many flavoring nowadays and keeps extremely well in the freezer if you have made in a large batch.";
  	  }
  	  else if(this.id == 31)
  	  {
  	  	this.image = "assets/img/kuih31.jpg";
  	  	this.name = "Nona Manis";
  	  	this.detail = "";
  	  }
  	  else if(this.id == 32)
  	  {
  	  	this.image = "assets/img/kuih32.jpg";
  	  	this.name = "Kuih Ondeh-Ondeh";
  	  	this.detail = "Kuih Ondeh-ondeh or also known as Buah Melaka refers to a kind of kuih made from ingredients such as glutinous rice flour, candy, grated young coconut, salt, water and pandan leaves (for fragrant colours and scents). Kuih Ondeh-ondeh is a popular dish in Malaysia. In Indonesia it is known as Klepon. In Brunei it is known as kuih pancit. Kuih Ondeh-Ondeh name comes from the way it produces where the dough is rounded. Kuih Ondeh-Ondeh comes from its size and shape resembling a fruit tree. In addition, the difference between the ondeh-ondeh and Melaka fruit cakes is its main content. The Melaka fruit cake uses Melaka sack / sugar as brighter as the FILLING. The ondeh-ondeh cake uses gravy sugar made from coconut nira and darker black.";
  	  }
  	  else if(this.id == 33)
  	  {
  	  	this.image = "assets/img/kuih33.jpg";
  	  	this.name = "Kuih Tepung Pelita";
  	  	this.detail = "Kuih tepung pelita is a kind of traditional kuih that is delicious and delicious. Kuih tepung pelita is a sweet pastry but not durable. Kuih tepung pelita are basically made of coconut milk and rice flour and cooked in containers made from banana leaves. In the middle it is filled with sugar water that makes it a sweet fat. It is easily damaged and does not last long. Basically, the light bulb consists of two parts, the upper part or known as the head, soft and made of coconut milk and white. The bottom is made from rice flour and is usually greenish and slightly harder than the head but still soft. Popular in Malaysia, Indonesia, and Singapore. The kuih tepung pelita consists of two layers of green colour below and a layer of white milk on top, known as the head. It is a sweet kuih and is very much loved on the east coast.";
  	  }
  	  else if(this.id == 34)
  	  {
  	  	this.image = "assets/img/kuih34.jpg";
  	  	this.name = "Kuih Kaswi";
  	  	this.detail = "Kuih Kaswi refers to a popular cake tradition in Malaysia. It is usually a small cup with a small hole in the centre. It is usually a colourful cake. Kuih kaswi is a steamed cake, so it is healthy because it does not have oil. The cake is made from a mixture of rice flour and sweet potato. Available in brown or different colours. Kuih kaswi is a snack served with young grated coconut which is planted on it. Its name comes from a cake form that has a hollow or groove in the middle. It is also known as kuih lompang. Kuih Kaswi i is usually generated for daily sales. It usually do not last long, and need to be cooled for longer storage as they are served with grated coconut. Coconut needs to be isolated for storage. Usually it is produced with a variety of colours to enhance the excitement. The foam is ideal for being served as an afternoon meal because it is easy to serve, and it tastes good. Even though it has coconut which may be underestimated, it is easy to keep in order not to clutter.";
  	  }
  	  else if(this.id == 35)
  	  {
  	  	this.image = "assets/img/kuih35.jpg";
  	  	this.name = "Kuih Cara Berlauk";
  	  	this.detail = "Kuih Cara Berlauk is a Malay inspired dumpling cooked on a hot plate like the Japanese style dumplings called Takoyaki. It’s a savoury snack that can be prepared for breakfast or tea. Suitable to serve during special occasions. It is also great served as appetizer during a fancy party.";
  	  }
  	  else if(this.id == 36)
  	  {
  	  	this.image = "assets/img/kuih36.JPG";
  	  	this.name = "Kuih Cara Manis";
  	  	this.detail = "Kuih Cara Manis is a popular type of kuih tradition in Malaysia. It is a kind of kuih made from wheat flour mixed with flour and pandan leaf water. The dough is then mixed with coconut milk and stirred in order not to mix. Then it is put into mold and cooked. Sweet ways are popular around Malaysia. It's a lot made, and it's sold everywhere and does not need to be booked in advance. The popular way of making a cake is as a delicious cakes. However it should be noted that the outer part of the stick is also sticky to the fingers of the person eating it.";
  	  }
  	  else if(this.id == 37)
  	  {
  	  	this.image = "assets/img/kuih37.jpg";
  	  	this.name = "Apam Hitam Manis";
  	  	this.detail = "Apam Hitam Manis is a type of apam commonly found in Malaysia. It is made with wheat flour, egg, sugar, and butter. It name is from the dark colour and the sweet taste. It is a kind of cake that is not good, and can be eaten. It is widely sold in farmer's market and night market, and there are all over Malaysia. It is a kind of cake that is not good, and can be eaten only, but less popular, perhaps because it seems less interesting.";
  	  }
  	  else if(this.id == 38)
  	  {
  	  	this.image = "assets/img/kuih38.JPG";
  	  	this.name = "Kuih Bom";
  	  	this.detail = "Kuih Bom or also known as kuih bebijan is a kind of Malay kuih made from wheat flour and sweet coconut. Kuih Bom are round and coated with sesame seeds, and in them are placed in sweet coconut cinnamon, before frying with cooking oil using a slow fire until it is yellowish and fried, ready to be served. Kuih Bom was so shaped that the round shape was quite large, with the coconut filling inside it. It resembles a cane of tapioca fruit but is larger in size and has a sweet coconut FILLING inside it. There are also bombs made with black sesame seeds.";
  	  }
  	  else if(this.id == 39)
  	  {
  	  	this.image = "assets/img/kuih39.jpg";
  	  	this.name = "Kuih Lopes";
  	  	this.detail = "Kuih lopes is a traditional Malay kuih. It is made of steamed rice in steamer. Once cool it is cut and mixed with dried grated coconut.";
  	  }
  	  else if(this.id == 40)
  	  {
  	  	this.image = "assets/img/kuih40.jpg";
  	  	this.name = "Kuih Cek Mek Molek";
  	  	this.detail = "Kuih cek mek molek or better known as che mek is a very popular traditional kuih in Kelantan and Terengganu. It is made with salted sweet potatoes and wheat flour as batter, sugar as a nucleus, then fried. If it is not eaten carefully, the sugar core is easily spilled. Kuih cek mek molek are made in such a way as they are oval and sleek. Kuih cek mek molek are made and sold throughout Malaysia. It's usually sold on a daily basis, and it's easy to make the evening cakes. It is also sold in the night market.";
  	  }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailPage');
  }


  goIngredient(id) {
  	console.log("id ingredient", id);
    let modal = this.modalCtrl.create(IngredientPage, {id:id});
    modal.present();
  }

    goInstruction(id) {
    console.log("id instruction", id);
    let modal = this.modalCtrl.create(InstructionPage, {id:id});
    modal.present();
  }

}
